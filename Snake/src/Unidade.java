import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Unidade {
	/*
	 * Atributos
	 */
	public int x, y, tamanho;
	
	/*
	 * Construtor
	 */
	public Unidade (int tamanho) {
		this.tamanho = tamanho;
	}
	
	/* 
	 * Getters e Setters
	 */
	public int getX() { return x; }
	public int getY() { return y; }
	public void setX (int x) { this.x = x; }
	public void setY (int y) { this.y = y; }
	public void setXY (int x, int y) { this.x = x; this.y = y; } // Seta posi��o da unidade
	
	/* 
	 * Move a unidade na dire��o especifica nos eixos X e Y.
	 * 
	 * @param	dx	dire��o no eixo x
	 * @param	dy	dire��o no eixo y
	 * @return	null
	 * 
	 */
	public void  mover (int dx, int dy) { // Para movimentar a cobra
		x += dx;
		y += dy;
	}
	
	public Rectangle forma() {
		return new Rectangle ( x, y, tamanho, tamanho);
	}
	
	public boolean colidiu(Unidade outra) {
		if (outra == this) return false;
		else return forma().intersects(outra.forma());
	}
	
	public void renderizar(Graphics2D grafico) {
		grafico.fillRect(x + 1, y + 1, tamanho - 2, tamanho - 2);
	}
}
