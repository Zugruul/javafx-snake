import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class PainelJogo extends JPanel implements Runnable, KeyListener {

	/*
	 * Atributos
	 */
	public static final int LARGURA = 800; // determina largura do painel
	public static final int ALTURA = 640; // determina altura do painel
	// static para poder ser chamado sem ser instanciado de outras classes
	// final para que o valor n�o possa ser alterado, por ser final tamb�m a conven��o � que todos os caracteres do nome da vari�vel estejam em ma�sculo
	
	/*
	 * Gr�ficos do Jogo
	 */
	private Graphics2D grafico;
	private BufferedImage image;
	
	
	/*
	 * Thread, Estado do Jogo, FPS Alvo
	 */
	private Thread thread; // Cria um thread, rodar o c�digo em um thread permite que v�rios thread trabalhem paralelamente.
	private boolean rodando;
	private boolean gameover;
	private boolean restart;
	private long fpsAlvo; // frames por segundo alvo
	
	/*
	 * Configura��es
	 */
	private int dificuldadeMax = 12;
	private Color corCobra = Color.GREEN;
	private Color corMaca = Color.RED;
	private int tamInicial = 4; // altera o tamanho inicial do corpo no come�o do jogo
	private Font fonteNormal; // Iniciado ap�s os graficos em init()
	private Font fonteGrande;
	private Font fonteSuperGrande;
	private int multVeloc = 2;
	private int fpsInicial = 8;
	private boolean paredesMatam = false; // Se verdeiro a parede te mata quando atravessada. Caso contrario ela te leva pra parede oposta
	
	/*
	 * Cobra
	 */
	// propriedades imut�veis
	private final int tamGrafico = 40; // tamanho do pixel de gr�fico
	private final int tamMaximoCorpo = ((LARGURA*ALTURA)/tamGrafico) + 1; // alterar isso depois de c�lculado o numero de grids
	
	// corpo e tamanho
	private Unidade snakeHead;
	private Unidade[] snake;
	private int tamanho = 0;
	
	// movimenta��o
	private int dx, dy;
	private boolean w, a, s, d;
	private boolean isPaused;
	
	/*
	 * Maca
	 */
	private Unidade maca;
	int placar;
	int nivel;
	
	
	public PainelJogo() {
		setPreferredSize(new Dimension(LARGURA, ALTURA));
		setFocusable(true);
		requestFocus();
		addKeyListener(this);
	}
	
	@Override
	public void addNotify() { // M�todo de JComponent da qual JPanel herda que � invocada quando o componente se torna "aparentado"
		super.addNotify();
		thread = new Thread(this);
		thread.start();
	}
	
	/*
	 * Unidades do Jogo
	 */
	private void prepararFase() {
		snakeHead = new Unidade(tamGrafico); // Inicializa a cabe�a da cobra
		snake = new Unidade[tamMaximoCorpo]; // Inicializa o corpo da cobra
		snakeHead.setXY(LARGURA/2, ALTURA / 2); // coloca bem no meio
		tamanho = 0;
		snake[0] = snakeHead; tamanho++;
		for(int i = 1; i < tamInicial; i++) {
			/*Unidade e = new Unidade(tamGrafico);
			e.setXY(snakeHead.getX() + ((i) * tamGrafico), snakeHead.getY());
			snake[i] = e;
			tamanhoCorpo++;*/
			aumentarCorpo(i);
		}
		maca = new Unidade(tamGrafico);
		spawnMaca();
		gameover = false;
		restart = false;
		dx = dy = 0;
		nivel = 1;
		placar = 0;
		setFPS(fpsInicial + (nivel * multVeloc));
	}
	
	private void spawnMaca() {
		int x = (int)(Math.random() * (LARGURA - tamGrafico));
		int y = (int)(Math.random() * (ALTURA - tamGrafico));
		x = x - (x % tamGrafico); // Ajeita posi��o da ma�� na grade
		y = y - (y % tamGrafico); // Ajeita posi��o da ma�� na grade
		maca = new Unidade (tamGrafico);
		for(int i = 0; i < tamanho; i++ ) {
			if(snake[i].getX() == x && snake[i].getY() == y) {
				x = (int)(Math.random() * (LARGURA - tamGrafico));
				y = (int)(Math.random() * (ALTURA - tamGrafico));
				x = x - (x % tamGrafico); // Ajeita posi��o da ma�� na grade
				y = y - (y % tamGrafico); // Ajeita posi��o da ma�� na grade
				i = 0;
			}
		}
		maca.setXY(x, y);
	}
	
	private void aumentarCorpo(int index) {
		Unidade e = new Unidade(tamGrafico);
		e.setXY(snakeHead.getX() + ((index) * tamGrafico), snakeHead.getY());
		e.setXY(-tamGrafico*2, -tamGrafico*2); // spawna corpo fora do espa�o de jogo
		snake[index] = e;
		tamanho++;
	}
	
	/*
	 * M�todos Gr�ficos e de Frames
	 */
	
	private void atualizar() {
		// gameover
		
		if(gameover) {
			if(restart) {
				prepararFase();
			}
			return;
		}
		
		
		// movimento
		if(w && dy == 0 && !(s || a || d)) {
			dy = -tamGrafico; // pra cima diminui altura (tende a 0)
			dx = 0;
		}
		
		if(s && dy == 0 && !(w || a || d)) {
			dy = tamGrafico; // pra baixo aumenta altura (tende a ALTURA)
			dx = 0;
		}
		
		if(a && dx == 0 && !(w || s || d)) {
			dx = -tamGrafico; // para esquerda diminui largura (tende a 0)
			dy = 0;
		}
		
		if(d && dx == 0 && !(w || a || s)) { // para a esquerda aumenta largura (tende a LARGURA)
			dx = tamGrafico;
			dy = 0;
		}
		if((dx != 0 || dy != 0) && !isPaused) { // verifica se h� movimenta vertical ou horizontal
			for(int i = tamanho - 1; i > 0; i--) { // para cada peda�o do corpo
				snake[i].setXY(snake[i-1].getX(), snake[i-1].getY()); // movimenta o de tr�s at� a posi��o do atual
			}
			snakeHead.mover(dx, dy); // movimenta a cabe�a
			//System.out.println("x" + dx + ", y" + dy); // Debug
		}
		
		for(int i = 1; i < tamanho; i++) {
			if(snake[i].colidiu(snakeHead)) {
				gameover = true;
				break;
			}
		}
		
		if(!paredesMatam) {
			if(snakeHead.getX() < 0) snakeHead.setX(LARGURA - tamGrafico); // Ao chegar em um dos extremos a cobra pula pro outro lado
			if(snakeHead.getY() < 0) snakeHead.setY(ALTURA - tamGrafico);
			if(snakeHead.getX() >= LARGURA) snakeHead.setX(0);
			if(snakeHead.getY() >= ALTURA) snakeHead.setY(0);
			//System.out.println("Cobra Pos: " + snakeHead.getX() + "---" + snakeHead.getY());
		} else {
			if(snakeHead.getX() < 0) gameover = true; // Ao chegar em um dos extremos a cobra pula pro outro lado
			if(snakeHead.getY() < 0) gameover = true;
			if(snakeHead.getX() > LARGURA) gameover = true;
			if(snakeHead.getY() > ALTURA) gameover = true;
		}
		
		if(maca.colidiu(snakeHead)) {
			placar++;
			spawnMaca();
			aumentarCorpo(tamanho);
			if(placar % 10 == 0) { // verifica se o placar � dificil por 10
				nivel++; // se for o nivel aumenta
				if(nivel > dificuldadeMax) nivel = dificuldadeMax; // verifica se o nivel ultrapassou o nivel maximo
				setFPS(fpsInicial + (nivel * multVeloc)); // seta fps (aumenta velocidade do jogo)
			}
		}
	}
	
	private void solicitacaoRender() {
		renderizar(grafico);
		Graphics g = getGraphics();
		g.drawImage(image, 0, 0, null);
		g.dispose();
	}

	private void renderizar(Graphics2D grafico) {
		grafico.clearRect(0, 0, LARGURA, ALTURA);
		grafico.setColor(corCobra);
		for(int i = 0; i < tamanho; i++) {
			Unidade s = snake[i];
			s.renderizar(grafico);
		}
		
		grafico.setColor(corMaca);
		maca.renderizar(grafico);
		
		if(gameover) {
			grafico.setColor(Color.RED);
			grafico.setFont(fonteSuperGrande);
			grafico.drawString("Game Over!", LARGURA/3, ALTURA/2);
			grafico.setFont(fonteGrande);
			grafico.drawString("Pressione 'Enter' para Recome�ar!", LARGURA/3, ALTURA/2+30);
		}
		if(dy == 0 && dx == 0) {
			grafico.setColor(Color.WHITE);
			grafico.setFont(fonteSuperGrande);
			grafico.drawString("Prepare-se!", LARGURA/3, ALTURA/2);
		}
		if (isPaused) {
			grafico.setColor(Color.WHITE);
			grafico.setFont(fonteSuperGrande);
			grafico.drawString("Jogo Pausado", LARGURA/3, ALTURA/2);
		}
		
		
		grafico.setColor(Color.WHITE);
		grafico.setFont(fonteGrande);
		grafico.drawString("Placar: " + placar, 10, ALTURA-32);
		grafico.drawString("N�vel: " + nivel, 10, ALTURA-8);
		
		grafico.setColor(Color.WHITE);
		grafico.setFont(fonteNormal);
		grafico.drawString("'Espa�o' para pausar.", LARGURA*84/100, ALTURA*99/100);
	}
	

	/**
	 * Altera a Taxa de Quadros por Segundo do Jogo
	 *
	 * @param	fps		um valor de frames por segundo
	 * @return	null
	 */
	private void setFPS(int fps) {
		fpsAlvo = 1000 / fps;
	}
	
	/* 
	 * Controle de Inputs
	 */
	
	@Override
	public void keyPressed(KeyEvent tecla) {
		int k = tecla.getKeyCode(); // obtem codigo numero da tecla
		
		// Movimenta��o da Cobra
		if(k == KeyEvent.VK_UP || k == KeyEvent.VK_W) w = true; // checa codigo numerico digitado contra valor desejado para ir pra cima, mesmo para demais s� muda dire��o
		if(k == KeyEvent.VK_LEFT || k == KeyEvent.VK_A) a = true;
		if(k == KeyEvent.VK_DOWN || k == KeyEvent.VK_S) s = true;
		if(k == KeyEvent.VK_RIGHT || k == KeyEvent.VK_D) d = true;
		// Demais teclas
		if((k == KeyEvent.VK_ENTER) && gameover) restart = true; // Pausa o jogo
	}

	@Override
	public void keyReleased(KeyEvent tecla) {
		int k = tecla.getKeyCode(); // obtem codigo numero da tecla
		if(k == KeyEvent.VK_UP || k == KeyEvent.VK_W) w = false;
		if(k == KeyEvent.VK_LEFT || k == KeyEvent.VK_A) a = false;
		if(k == KeyEvent.VK_DOWN || k == KeyEvent.VK_S) s = false;
		if(k == KeyEvent.VK_RIGHT || k == KeyEvent.VK_D) d = false;
		if(k == KeyEvent.VK_SPACE) togglePause(); // Pausa o jogo
	}

	@Override
	public void keyTyped(KeyEvent tecla) {
		// m�todo herdado
	}
	
	public static boolean Salvar() {
		System.out.println("Iniciando processo de salvamento!");
		return true;
	}
	
	
	/*
	 * Execu��o
	 */
	
	private void init() {
		image = new BufferedImage(LARGURA, ALTURA, BufferedImage.TYPE_INT_ARGB);
		grafico = image.createGraphics();
		fonteNormal = grafico.getFont(); // Fonte normal para o placar
		fonteGrande = fonteNormal.deriveFont(fonteNormal.getSize() * 2F); // fonte grande para a tela inicial e de game over
		fonteSuperGrande = fonteNormal.deriveFont(fonteNormal.getSize() * 4F); // fonte grande para a tela inicial e de game over
		rodando = true;
		prepararFase();
	}
	
	@Override
	public void run() {
		if(rodando) return; // se o jogo estiver rodando encerra o m�todo
		
		init(); // se o jogo n�o estiver rodando o inicia
		long inicio; // tempo inicial
		long decorrido; // tempo atual
		long espera; // tempo de espera para alcan�ar a taxa de quadros por segundo desejada
		
		while(rodando) {
			inicio = System.nanoTime();
			atualizar();
			solicitacaoRender();
			decorrido = System.nanoTime() - inicio;
			espera = fpsAlvo - decorrido / 1000000;
			if(espera > 0) {
				try {
					Thread.sleep(espera);
				} catch (Exception e) {
					System.err.println(e.getMessage());
				}
			}
		}
	}
	
	public void togglePause() {
		isPaused = !isPaused;
	}
}
