import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;

public class Main {
	public static void main(String[] args) {
		JFrame frame = new JFrame("Snake"); // Cria frame sob o nome "Snake"
		frame.setContentPane(new PainelJogo()); // O conte�do dessa Frame se torna um PainelJogo (Painel de Jogo por heran�a � um JPanel)
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Determina que nada acontece ao apertar para fechar a janela
		frame.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent event) {
				PainelJogo.Salvar();
			}
		});
		frame.setResizable(false); // Torna o tamanho da janela inalter�vel
		frame.pack(); // Causa a janela a tomar o tamanho ideal para acomodar seus componentes
		frame.setPreferredSize(new Dimension(PainelJogo.LARGURA, PainelJogo.ALTURA)); // Muda o tamanho preferencial da janela para o mesmo do Painel de Jogo
		frame.setVisible(true); // Torna a Janela vis�vel
	}
}
